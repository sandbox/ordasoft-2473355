About Master theme:
------------------
Master Drupal theme is fully responsive and fits to all screen sizes. 
OS Master is not dependent on any core theme, has modern and clean design. 
Theme is perfect for any kind of portfolio and personal website,
 corporate and company site
or other small business website.


Drupal compatibility:
---------------------
Master theme is compatible with Drupal 7.x.x


Browser compatibility:
----------------------
Compatible with IE8+, Opera, Firefox, Chrome browsers


INSTALLATION:
-------------
1. Download Master from Drupal directory
2. Unpack the downloaded file, take the folders and place them in your
  Drupal installation under following 
  location: sites/all/themes
3. Make this theme default in Drupal admin area, tab Appearance
4. Download modules FancyBox, jQuery Update,
  Libraries API from Drupal.org and install their
5. Module FancyBox unpack take the folders and place them in your
  Drupal under following 
  location: sites/all/library
 

Developed & designed by:
------------------------
Ordasoft - http://ordasoft.com


Support:
--------
Ordasoft support forum - http://ordasoft.com/Forum/

Documentation for template
--------------------------
http://ordasoft.com/template_documentation/drupal-master/
