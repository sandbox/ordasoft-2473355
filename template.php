<?php
/**
 * @file
 * Theme settings.
 *
 * Filename:     theme-settings.php.
 * Website:      http://www.ordasoft.com
 * Author:       ordasoft dev team ordasoft.com.
 */

/**
 * Override or insert variables into the page template.
 */
function os_master_preprocess_page(&$vars) {

  $libmass = array(
    "libraries" => "Libraries",
    "admin_menu" => "Admin menu",
    "advanced_help" => "Advanced help",
    "contact_form_blocks" => "Contact form blocks",
    "ctools" => "Ctools",
    "imageblock" => "Imageblock",
    "media" => "Media",
    "multiform" => "Multiform",
    "navigation404" => "Page 404",
    "panels" => "Panels",
    "views" => "Views",
    "wysiwyg" => "Wysiwyg",
  );
  $no_modules = '';
  foreach ($libmass as $key => $value) {
    if (!module_exists($key)) {
      // No needed_module means we need to add it, let everyone know.
      if ($no_modules) :
        $no_modules .= ', ';
      endif;
      $no_modules .= $value;
    }
  }
  if ($no_modules) {
    drupal_set_message(t("os_master requires that module $no_modules installed")
                           . "<h6>" . t('warning generated in file: %f', array('%f' => __FILE__)) . "</h6>", 'warning');
  }

  if (drupal_is_front_page()) {
    unset($vars['page']['content']['system_main']['default_message']);
  }

  if (module_exists('color')) {
    _color_page_alter($vars);
  }

  $gmass = array(
    "show_hide_icon",
    "show_hide_copyright",
  );

  foreach ($gmass as $value) {
    $vars[$value] = theme_get_setting($value, 'os_master');
  }

  os_master_prepere_social_icons($vars);
}


/**
 * Override or insert variables into the social icons.
 */
function os_master_prepere_social_icons(&$vars) {
  $gmass = array(
    "twitter",
    "facebook",
    "flickr",
    "linkedin",
    "youtube",
    "pinterest",
    "google",
    "dribbble",
    "vimeo",
    "instagram",
    "vk",
  );

  $social_icons = array();
  foreach ($gmass as $value) {
    $social_icons["fa-" . $value] = theme_get_setting($value, 'os_master');
  }

  $vars["social_icons"] = "";
  foreach ($social_icons as $key => $value) {
    if (trim($value) != "") :
       $vars["social_icons"] .= '<li><a href="' . $value . '" target="_blank">
          <i class="fa ' . $key . '"></i> </a></li>';
    endif;
  }

}

/**
 * Override or insert variables into the main menu.
 */
function os_master_links__system_main_menu($vars) {
  $pid = variable_get('menu_main_links_source', 'main-menu');
  $tree = menu_tree($pid);
  return drupal_render($tree);
}

/**
 * Override or insert variables into the top_menu.
 */
function os_master_links__system_top_menu($vars) {
  $pid = variable_get('menu_top_links_source', 'top-menu');
  $tree = menu_tree($pid);
  return drupal_render($tree);
}

/**
 * Override or insert variables into the secondary_menu.
 */
function os_master_links__system_secondary_menu(&$vars) {
  $pid = variable_get('menu_secondary_links_source', 'menu-secondary-menu');
  $tree = menu_tree($pid);
  return drupal_render($tree);
}

/**
 * Override or insert fields output node.
 */
function os_master_get_field_value($vars, $node) {
  $icon = field_get_items('node', $node, $vars);
  $icon_show = field_view_value('node', $node, $vars, $icon[0]);
  return drupal_render($icon_show);
}

/**
 * Override or insert variables into the setting template.
 */
function os_master_process_html(&$vars) {
  if (module_exists('color')) {
    _color_html_alter($vars);
  }

  $tmas = array(
    "b_decor",
    "m_decor",
    "f_decor",
    "b_decor_hover",
    "m_decor_hover",
    "f_decor_hover",
    "layout_pattern",
    "body_font",
    "main_menu_font",
    "body_links_font",
    "footer_links_font",
    "h1_font",
    "h2_font",
    "h3_font",
    "h4_font",
    "h5_font",
    "h6_font",
  );
  foreach ($tmas as $value) {
    $vars[$value] = theme_get_setting($value, 'os_master');
  }
}
