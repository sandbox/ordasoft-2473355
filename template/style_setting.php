<?php
/**
 * @file
 * Style settings. Override or insert style theme settings.
 *
 * Filename:     style_stting.css
 * Website:      http://www.ordasoft.com
 * Author:       ordasoft dev team ordasoft.com.
 */

?>

<style type="text/css">

body {
  background-repeat: repeat;
  font-family: <?php print $body_font ?>;
}
a {
  font-family: <?php print $body_links_font ?>;
  text-decoration: <?php print (($b_decor == 1) ? 'underline' : 'none'); ?>;
}
a:hover {
    text-decoration: <?php print (($b_decor_hover == 1) ? 'underline' : 'none'); ?>;
}
.mainMenu li a {
  font-family: <?php print $main_menu_font ?>;
  text-decoration: <?php print (($m_decor == 1) ? 'underline' : 'none'); ?>;
}
.mainMenu li a:hover {
    text-decoration: <?php print (($m_decor_hover == 1) ? 'underline' : 'none'); ?>;
}
#footer a {
  font-family: <?php print $footer_links_font ?>;
  text-decoration: <?php print (($f_decor == 1) ? 'underline' : 'none'); ?>;
}
#footer a:hover {
    text-decoration: <?php print (($f_decor_hover == 1) ? 'underline' : 'none'); ?>;
}

h1 {font-family: <?php print $h1_font ?> !important;}
h2 {font-family: <?php print $h2_font ?> !important;}
h3 {font-family: <?php print $h3_font ?> !important;}
h4 {font-family: <?php print $h4_font ?> !important;}
h5 {font-family: <?php print $h5_font ?> !important;}
h6 {font-family: <?php print $h6_font ?> !important;}
</style>
